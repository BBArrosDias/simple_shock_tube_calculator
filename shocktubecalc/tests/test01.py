import unittest
import sys
sys.path.append('../shocktubecalc')
import sod


class TestSodShockTube(unittest.TestCase):
    def test_standard_sod(self):
        positions, regions, values = sod.solve(left_state=(1, 1, 0), right_state=(0.125, 0.1, 0.),
                                               geometry=(-1., 1., 0), t=0.2, gamma=1.4, npts=500)

        self.assertAlmostEqual(positions['Head of Rarefaction'], -2.3664320E-01)
        self.assertAlmostEqual(positions['Foot of Rarefaction'], -1.6301263E-02)
        self.assertAlmostEqual(positions['Contact Discontinuity'], 1.8361829E-01)
        self.assertAlmostEqual(positions['Shock'], 3.9676762E-01)

        for calc, exact in zip(regions['Region 1'], (1, 1, 0)):
            self.assertAlmostEqual(calc, exact)

        self.assertAlmostEqual(regions['Region 2'], 'RAREFACTION')

        for calc, exact in zip(regions['Region 3'], (3.0713445E-01, 4.3033445E-01, 9.1809142E-01)):
            self.assertAlmostEqual(calc, exact)

        for calc, exact in zip(regions['Region 4'], (3.0713448E-01, 1.8614540E-01, 9.1809142E-01)):
            self.assertAlmostEqual(calc, exact)

        for calc, exact in zip(regions['Region 5'], (1.2500000E-01, 1.0000000E-01, 0.0000000E+00)):
            self.assertAlmostEqual(calc, exact)

    def test_large_shock(self):
        positions, regions, values = sod.solve(left_state=(1000, 1000, 0), right_state=(1, 1, 0.),
                                               geometry=(-1., 1., 0.), t=0.1, gamma=1.4, npts=500)

        self.assertAlmostEqual(positions['Head of Rarefaction'], -0.11832159566199232)
        self.assertAlmostEqual(positions['Foot of Rarefaction'], 0.21689234591382475)
        self.assertAlmostEqual(positions['Contact Discontinuity'], 0.2793449513131809)
        self.assertAlmostEqual(positions['Shock'], 0.37277055590159003)

        for calc, exact in zip(regions['Region 1'], (1000, 1000, 0)):
            self.assertAlmostEqual(calc, exact)

        self.assertAlmostEqual(regions['Region 2'], 'RAREFACTION')

        for calc, exact in zip(regions['Region 3'], (11.4131572789317, 40.96686359058944, 2.7934495131318084)):
            self.assertAlmostEqual(calc, exact)

        for calc, exact in zip(regions['Region 4'], (11.4131572789317, 3.9900256203194853, 2.7934495131318084)):
            self.assertAlmostEqual(calc, exact)

        for calc, exact in zip(regions['Region 5'], (1, 1, 0)):
            self.assertAlmostEqual(calc, exact)


if __name__ == '__main__':
    unittest.main()

